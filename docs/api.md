# API Docs

The REST API can be accessible at port `60881` unless overriden via the `PORT` variable, listening on all interfaces, so please adjust your firewall settings accordingly.

**:warning: The API docs is a work in progress.** We're currently ironing out the backend code behind the scenes, much like the stability status of the Node.js code in this code.

## Endpoints

**Base URL**: `http://localhost:${PORT:-"60881"}` (for non-Linux users, defaults to `60881` unless `PORT` variable is defined when running `yarn start`)

### `/getState`

**Description**: Gets the current RPC state from `rpcState.json` file.

**Example response**:

```jsonc
// curl http://localhost:60881/getState | jq .
{
  "details": "On the Admin Stream",
  "state": "https://twitch.tv/TheNoxcrew",
  "assets": {
    "large_image": "mcc-twitter",
    "large_text": "MC Championship",
    "small_image": "spectator",
    "small_text": "Admin Stream"
  },
  "buttons": [
    {
      "label": "Check event stats",
      "url": "https://mcc.live"
    },
    {
      "label": "Use this Custom RPC",
      "url": "https://gitlab.com/RecapTime/mcc-discord-rpc"
    }
  ],
  "timestamps": {
    "start": 1647961984753
  }
}
```

### `/changeTeams/regular-<participant|viewer>/<teamSlug>`

## Appendix