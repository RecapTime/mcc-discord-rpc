# Available Assets for Rich Presence use

**COPYRIGHT DISCLAIMER**: These assets listed below are used for use in the Rich Presence stuff and doesn't intend to violate the copyright of these assets from Noxcrew.

| Key | URL from Discord's CDN / Preview | Source |
| --- | --- | --- |
| `mcc-twitter` | ![MCC Twitter pfp](https://cdn.discordapp.com/app-assets/952456760948559932/952473393922449418.png) | Twitter |
| `mcc-logo` | Same as `mcc-twitter` curently, might change soon | |
| `spectator` | ![Admin Stream icon](https://cdn.discordapp.com/app-assets/952456760948559932/952579352032329728.png) | Straight from the main stats website, through the magic of DevTools.
| `red-rabbits` | ![Red Rabbits](https://cdn.discordapp.com/app-assets/952456760948559932/952458916078428191.png) | MCC Wiki
| `orange-ocelots` | ![Orange Ocelots](https://cdn.discordapp.com/app-assets/952456760948559932/955451556977868850.png) | MCC Wiki
| `yellow-yaks` | ![Yellow Yaks](https://cdn.discordapp.com/app-assets/952456760948559932/955451556948500560.png) | MCC Wiki
| `` | !
| ``
| ``
| ``
| ``
| ``
| ``