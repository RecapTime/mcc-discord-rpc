# Unofficial MCC Discord Rich Presence

| Key | Value |
| --- | --- |
| Maintability/Stability Status | New Project / [![experimental](http://badges.github.io/stability-badges/dist/experimental.svg)](http://github.com/badges/stability-badges) |
| Description | Unofficial Discord Rich Presence for MCC viewers/participants, in an REST API.
| License | MIT |
| Maintainer | [Andrei Jiroh](https://ajhalili2006.bio.link) |
| Canonical Repository | <https://gitlab.com/RecapTime/mcc-discord-rpc>

Contains programmatic REST API to update Discord Rich Presence on the fly, especially for MCC viewers who use Discord. It's still not yet ready for production use, but we welcome accepting community contributions

## Requirements

You need [Node.js 16 (or later)](https://nodejs.org) and optionally [Git](https://git-scm.com) if you choose not to [download the source tarball](https://gitlab.com/RecapTime/mcc-discord-rpc/-/archive/main/mcc-discord-rpc-main.zip) (and want to easily update your copy).

## Setup

1. Make sure Yarn is installed, and if not try `corepack enable && yarn` (it's all same for PowerShell and Command Prompt).
2. Navigate to your local copy of the repo (either through `git clone https://gitlab.com/RecapTime/mcc-discord-rpc` or extracted from tarball) and just run `yarn start`. No more `npm install` since we use Yarn Plug 'n Play in combination with offline caches checked in, especially in cases where Windows Defender will think Node.js processes are doing some ransomware-like activity on filesystem level when populating the `node_modules` directory via the regular npm install.
3. Fire up Discord desktop client and enjoy. Optionally hook up your stream board to [do some API calls](./docs/api.md) behind the scenes.

## Documentation

Additional documentation can be found on [the `docs` directory](./docs/README.md), including asset key names and API Docs.
