const { timestampStart } = require("./utils")

let teamsIndex = {
    regularEvents: {
        spectator: {
            details: "On the Admin Stream",
            state: "https://twitch.tv/TheNoxcrew",
            assets: {
                large_image: "mcc-twitter",
                large_text: "MC Championship",
                small_image: "spectator",
                small_text: "Admin Stream"
            },
            buttons: [
                {
                    "label": "Check event stats",
                    "url": "https://mcc.live"
                },
                {
                    "label": "Use this Custom RPC",
                    "url": "https://gitlab.com/RecapTime/mcc-discord-rpc"
                }
            ],
            timestamps: {
                start: timestampStart()
            }
        },
        red: {
            description: "Participating as a Red Rabbit",
            state: "Join me over the stream",
            assets: {
                large_image: "mcc-twitter",
                large_text: "MC Championship",
                small_image: "red-rabbits",
                small_text: "Red Rabbit"
            },
            buttons: [
                {
                    "label": "Check event stats / Where To Watch",
                    "url": "https://mcc.live"
                },
                {
                    "label": "Use this Custom RPC",
                    "url": "https://gitlab.com/RecapTime/mcc-discord-rpc"
                }
            ],
            timestamp: {
                start: timestampStart()
            }
        }
    },
    specialEvents: {}
}

let teamsWatchingIndex = {}

module.exports = {
    teamsIndex,
    teamsWatchingIndex
}