const { timestampStart } = require("./utils")

let defaultActivity = {
    details: "Event starts soon",
    assets: {
        large_image: "mcc-twitter",
        large_text: "MC Championship",
        small_image: "spectator",
        small_text: "Probably watching on Admin Stream soon"
    },
    state: "Idling behind the scenes",
    buttons: [
        {
            "label": "Check event stats",
            "url": "https://mcc.live"
        },
        {
            "label": "Use this Custom RPC",
            "url": "https://gitlab.com/RecapTime/mcc-discord-rpc"
        }
    ],
    timestamps: {
        start: timestampStart()
    }
}

module.exports = {
    defaultActivity
}
